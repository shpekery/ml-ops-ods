FROM python:3.12-slim

WORKDIR /app
COPY poetry.lock pyproject.toml ./

RUN apt update
RUN apt-get -y install gcc

# Poetry & deps
RUN python -m pip install --no-cache-dir poetry==1.8.2 \
    && poetry config virtualenvs.in-project true \
    && poetry install --with dev,test --no-interaction --no-ansi

# Quarto
RUN apt-get update && apt-get install -y wget
RUN wget "https://quarto.org/download/latest/quarto-linux-amd64.deb" -O quarto.deb
RUN dpkg -i quarto.deb

#FROM python:3.12-slim AS builder
#
#WORKDIR /app
#COPY poetry.lock pyproject.toml ./
#
#RUN python -m pip install --no-cache-dir poetry==1.8.2 \
#    && poetry config virtualenvs.in-project true \
#    && poetry install --without dev,test --no-interaction --no-ansi

#FROM python:3.11-slim
#
#COPY --from=builder /app /app
#COPY . ./
#
#CMD ["/app/.venv/bin/python", "-m",  "src.main"]
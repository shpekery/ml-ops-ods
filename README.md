## Работа с проектом

### Установка зависимостей
- Установить poetry: [официальная документация](https://python-poetry.org/docs/#installing-with-the-official-installer)

- Установить зависимости (команду следует исполнять в корневой папке проекта):

  ```shell
  poetry install
  ```

- Если у вас poetry не находит python версии 3.12, то надо сначала вручную найти интерпретатор python версии 3.12 с помощью команды (специфично для Windows):

  ```shell
  where python3
  ```

- После этого выполнить:

  ```shell
  poetry env use "путь/до/python3.12"
  ```

- И повторить:

  ```shell
  poetry install
  ```

### Pre-commit

- Инициализировать `pre-commit` можно выполнив следующую команду в корне вашего проекта:
  ```shell
  pre-commit install
  ```
- Отформатировать код в соответствии с принятым стилем можно выполнив следующую команду в корне вашего проекта:
  ```shell
  pre-commit run --all-files
  ```

### Тестирование
**Запуск тестов (когда и если они будут):**

```shell
poetry run pytest -v
```

## Запуск через Docker

- Сборка образа:

    ```shell
    docker build -t ml-ops-ods .
    ```

- Запуск контейнера:

    ```shell
    docker run ml-ops-ods
    ```

## Работа с документацией
Для работы с документацией необходимо установить quarto: [официальная документация](https://quarto.org/docs/get-started/)

Preview документации (dev сервер):

```shell
quarto preview docs
```

Сборка:

- Сборка docstring:

    ```shell
    quartodoc build --config="docs/_quarto.yml"
    ```

- Рендер документации:

    ```shell
    quarto render docs
    ```

## Методология
Для ведения репозитория мы выбрали методологию [Data Science Lifecycle Process](https://github.com/dslp/dslp), будем стараться ей следовать.
DATASETS = ["2015-street-tree-census-tree-data"]
STRATEGIES = ["cut_columns", "keep_columns"]
MODELS = ["boosting", "lasso"]

rule all:
    input:
        expand("data/{model}__{dataset}__{strategy}.pkl", dataset=DATASETS, strategy=STRATEGIES, model=MODELS),
        expand("data/images/{dataset}__corr_matrix.png", dataset=DATASETS),

rule read_dataset:
    input:
        "data/{dataset}.csv"
    output:
        "data/images/{dataset}__corr_matrix.png"
    shell:
        "python src/write_dataset_info.py --dataset_path {input} --image_path {output}"

rule preprocess:
    input:
        "data/{dataset}.csv"
    output:
        "data/preprocessed_{dataset}__{strategy}_train.csv",
        "data/preprocessed_{dataset}__{strategy}_test.csv"
    shell:
        "python src/preprocess_script.py --dataset_path {input} --train_output {output[0]} --test_output {output[1]} --strategy {wildcards.strategy}"

rule train_model:
    input:
        "data/preprocessed_{dataset}__{strategy}_train.csv",
        "data/preprocessed_{dataset}__{strategy}_test.csv"
    output:
        "data/{model_type}__{dataset}__{strategy}.pkl"
    threads: workflow.cores * 0.5
    shell:
        "python src/train_model.py model={wildcards.model_type} params.train_input={input[0]} params.test_input={input[1]} params.model_path={output}"

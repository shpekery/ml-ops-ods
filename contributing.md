## Работа с проектом

### Установка зависимостей
- Установить poetry: [официальная документация](https://python-poetry.org/docs/#installing-with-the-official-installer)

- Установить зависимости (команду следует исполнять в корневой папке проекта):

  ```shell
  poetry install
  ```

### Pre-commit

- Инициализировать `pre-commit` можно выполнив следующую команду в корне вашего проекта:
  ```shell
  pre-commit install
  ```
- Отформатировать код в соответствии с принятым стилем можно выполнив следующую команду в корне вашего проекта:
  ```shell
  pre-commit run --all-files
  ```

## Методология
Для ведения репозитория мы выбрали методологию [Data Science Lifecycle Process](https://github.com/dslp/dslp), будем стараться ей следовать.
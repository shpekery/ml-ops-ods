import argparse
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OrdinalEncoder

from src.config import compose_config


def preprocess_dataset(dataset, delete_columns):
    numeric_columns = dataset._get_numeric_data().columns
    categorical_columns = list(set(dataset.columns) - set(numeric_columns))

    for column in numeric_columns:
        dataset[column] = dataset[column].fillna(dataset[column].median())

    for column in categorical_columns:
        distribution = dataset[column].value_counts(normalize=True)
        missing = dataset[column].isnull()
        dataset.loc[missing, column] = np.random.choice(
            distribution.index,
            size=len(dataset[missing]),
            p=distribution.values,
        )

    if delete_columns:
        columns_to_delete = {
            "st_assem",
            "bbl",
            "borocode",
            "boro_ct",
            "longitude",
            "latitude",
            "cncldist",
            "bin",
        }
        new_columns = list(set(dataset.columns) - columns_to_delete)
        dataset = dataset[new_columns]

    X = dataset.drop("root_stone", axis=1)
    X_train, X_test, y_train, y_test = train_test_split(
        X, dataset["root_stone"], test_size=0.1, random_state=42
    )

    ordinal_encoder = OrdinalEncoder(
        handle_unknown="use_encoded_value", unknown_value=683789
    )
    numeric_columns = X_train._get_numeric_data().columns
    categorical_columns = list(set(X_train.columns) - set(numeric_columns))
    X_train[categorical_columns] = ordinal_encoder.fit_transform(
        X_train[categorical_columns]
    )
    X_test[categorical_columns] = ordinal_encoder.transform(
        X_test[categorical_columns]
    )
    target_encoder = OrdinalEncoder()
    y_train_np = y_train.to_numpy().reshape((len(y_train), 1))
    y_test_np = y_test.to_numpy().reshape((len(y_test), 1))
    y_train = target_encoder.fit_transform(y_train_np)
    y_test = target_encoder.transform(y_test_np)

    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    X_train_scaled = np.hstack((X_train_scaled, y_train))
    X_test_scaled = np.hstack((X_test_scaled, y_test))

    train_df = pd.DataFrame(X_train_scaled)
    test_df = pd.DataFrame(X_test_scaled)
    train_df.drop(train_df.columns[0], axis=1, inplace=True)
    test_df.drop(test_df.columns[0], axis=1, inplace=True)

    return train_df, test_df


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="This program redacts dataset -_-"
    )
    parser.add_argument(
        "--dataset_path", type=str, help="Relative path to validation dataset"
    )
    parser.add_argument(
        "--train_output",
        type=str,
        help="Path to the file in which train part of dataset will be written",
    )
    parser.add_argument(
        "--test_output",
        type=str,
        help="Path to the file in which train part of dataset will be written",
    )
    parser.add_argument(
        "--strategy",
        type=str,
        choices=["cut_columns", "keep_columns"],
        help="Type of model to train",
        default="boosting",
    )

    args = parser.parse_args()
    dataset = pd.read_csv(args.dataset_path)

    prepr_conf = compose_config(overrides=[f"preprocessing={args.strategy}"])[
        "preprocessing"
    ]

    delete_columns = prepr_conf["strategy"] == "cut_columns"
    X_train, X_test = preprocess_dataset(dataset, delete_columns)
    X_train.to_csv(args.train_output, index=False)
    X_test.to_csv(args.test_output, index=False)

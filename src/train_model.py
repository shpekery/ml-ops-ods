import hydra
import pandas as pd
from omegaconf import DictConfig
from sklearn.metrics import mean_squared_error
import joblib

from hydra.utils import instantiate


def train_model(X_train, X_test, model_cfg, cores):
    y_train = X_train.iloc[:, -1:]
    y_train = y_train.to_numpy().reshape(
        len(y_train),
    )
    X_train = X_train.drop(X_train.columns[-1], axis=1)

    y_test = X_test.iloc[:, -1:]
    y_test = y_test.to_numpy().reshape(
        len(y_test),
    )
    X_test = X_test.drop(X_test.columns[-1], axis=1)

    # if model_type == "boosting":
    #     model = HistGradientBoostingRegressor()
    # elif model_type == "lasso":
    #     model = Lasso()

    model = instantiate(model_cfg)

    params = model.get_params()
    if "n_jobs" in params:
        model.set_value(n_jobs=cores)

    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    print(mean_squared_error(y_test, y_pred))
    return model


@hydra.main(version_base=None, config_path="conf", config_name="config")
def main(cfg: DictConfig):
    # parser = argparse.ArgumentParser(
    #     description="This program redacts dataset -_-"
    # )
    # parser.add_argument(
    #     "--train_input",
    #     type=str,
    #     help="Path to the file in which train part of dataset will be written",
    # )
    # parser.add_argument(
    #     "--test_input",
    #     type=str,
    #     help="Path to the file in which train part of dataset will be written",
    # )
    # parser.add_argument(
    #     "--model_type",
    #     type=str,
    #     choices=["boosting", "lasso"],
    #     help="Type of model to train",
    #     default="boosting",
    # )
    # parser.add_argument(
    #     "--model_path",
    #     type=str,
    #     help="Path to file in which model will be saved",
    # )
    # args = parser.parse_args()

    params = cfg["params"]

    X_train = pd.read_csv(params["train_input"])
    X_test = pd.read_csv(params["test_input"])

    cores = int(params["cores"]) // 2
    model = train_model(X_train, X_test, cfg["model"], cores)
    joblib.dump(model, params["model_path"])


if __name__ == "__main__":
    main()

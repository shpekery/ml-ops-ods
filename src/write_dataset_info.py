import argparse
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def print_dataset(dataset, output_path):
    print("First 5 rows of the dataset")
    print(dataset.head())
    print("Statistics info of some dataset columns")
    print(dataset.describe())
    numerical_data = dataset.select_dtypes(include=["float64", "int64"])
    correlation_matrix = numerical_data.corr()
    plt.figure(figsize=(15, 12))
    sns.heatmap(correlation_matrix, annot=True)
    plt.savefig(output_path, bbox_inches="tight")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="This program prints dataset info"
    )
    parser.add_argument("--dataset_path", type=str, help="Path to dataset")
    parser.add_argument(
        "--image_path", type=str, help="Path to corr matrix image"
    )

    args = parser.parse_args()
    dataset = pd.read_csv(args.dataset_path)

    print_dataset(dataset, args.image_path)
